var port = process.argv[2];
var filename = process.argv[3];
var express = require('express');
var file = require("fs");
var app = express();

app.get('/books', function(req, res) {
    file.readFile(filename, 'utf8', function(err, data) {
        if (err) throw err;
        var out = JSON.parse(data);
        res.json(out)
    });
});

app.listen(port);