 var port = process.argv[2];
 var path=process.argv[3];
 var express = require('express');

 var app = express();

 app.use(require('stylus').middleware(path));
 app.use(express.static(path));
 app.get(path, function(req, res) {
     res.render('main');
 })
 app.listen(port);