var port = process.argv[2];
var express = require('express');
var app = express();

var hash = function(data)
{
    return require('crypto')
    .createHash('sha1')
    .update(new Date().toDateString() + data)
    .digest('hex');
}

app.put('/message/:NAME', function(req, res) {
  var id = req.params.NAME;
  res.send(hash(id));
});

app.listen(port);