 var net = require('http');
 var fs = require('fs');
 var port = parseInt(process.argv[2]);
 var file = process.argv[3];

 var server = net.createServer(function(request, response) {
     var result = fs.createReadStream(file);
     result.pipe(response);
 });
 server.listen(port);