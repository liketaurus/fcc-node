      __               _____           _      _____                       
     / _|             /  __ \         | |    /  __ \                      
    | |_ _ __ ___  ___| /  \/ ___   __| | ___| /  \/ __ _ _ __ ___  _ __  
    |  _| '__/ _ \/ _ \ |    / _ \ / _` |/ _ \ |    / _` | '_ ` _ \| '_ \ 
    | | | | |  __/  __/ \__/\ (_) | (_| |  __/ \__/\ (_| | | | | | | |_) |
    |_| |_|  \___|\___|\____/\___/ \__,_|\___|\____/\__,_|_| |_| |_| .__/ 
                                                                   | |    
                                                                   |_|    

Hi there!

This is my basic node.js code exercises, written in order to took a part in 
**[freeCodeCamp back-end certification](https://www.freecodecamp.com/challenges/claim-your-back-end-development-certificate)**.

If you want to try it yorself, install:

* this [interactive tutorial](https://github.com/workshopper/learnyounode) for **node.js** exercises
* this [interactive tutorial](https://github.com/FreeCodeCamp/fcc-expressworks) for **express.js** exercises
* this [interactive tutorial](https://github.com/evanlucas/learnyoumongo) for **MongoDB** exercises

View more tutorials at [nodeschool.io](http://nodeschool.io/)
