var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/learnyoumongo';
var firstName = process.argv[2];
var lastName = process.argv[3];

var doc = {
    firstName: firstName,
    lastName: lastName
};

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    }
    else {
        //console.log('Connection established to', url);
        db.collection('docs').insert(doc, function(err, data) {
            if (err) throw err
            else {
                console.log(JSON.stringify(doc));
                db.close();
            }
        })
    }
});