var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var database = process.argv[2];
var url = 'mongodb://localhost:27017/' + database;
var collection = process.argv[3];
var _id = process.argv[4];

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    }
    else {
        //console.log('Connection established to', url);
        db.collection(collection).remove({
            _id: _id
        }, function(err) {
            if (err) throw err
            else {
                db.close();
            }
        })
    }
});