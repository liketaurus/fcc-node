var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var size = process.argv[2];
var url = 'mongodb://localhost:27017/learnyoumongo';

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    }
    else {
        //console.log('Connection established to', url);
        db.collection('prices').aggregate([
            {$match: {size: size}},
            {$group:{_id:'average', average:{$avg: '$price'}}}
            ]
            ).toArray(function(err, results) {
            if (err) throw err;
            var average = Number(results[0].average).toFixed(2);
            console.log(average);
            db.close();
        })
    }
});