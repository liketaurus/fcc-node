var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/learnyoumongo';
var param = parseInt(process.argv[2]);

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    }
    else {
        //console.log('Connection established to', url);
        db.collection('parrots').find({
            age: {
                $gt: param
            }
        }, {
            name: 1,
            age: 1,
            _id: 0
        }).toArray(function(err, docs) {
            if (err) throw err;
            console.log(docs);
            db.close();
        })
    }
});