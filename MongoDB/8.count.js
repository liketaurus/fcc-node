var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var age = parseInt(process.argv[2]);
var url = 'mongodb://localhost:27017/learnyoumongo';

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    }
    else {
        //console.log('Connection established to', url);
        db.collection('parrots').count({
            age: {
                $gt: age
            }
        }, function(err,count) {
            if (err) throw err
            else {
                console.log(count);
                db.close();
            }
        })
    }
});