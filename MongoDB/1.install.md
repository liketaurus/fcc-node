# Installing MongoDB on a Cloud9 workspace

To install MongoDB in your workspace, you can open a terminal and run the following command:

    sudo apt-get install -y mongodb-org

## Running MongoDB on a Cloud9 workspace

To run MongoDB, run the following below (passing the correct parameters to it). Mongodb data will be stored in the folder data.

    mkdir data
    echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
    chmod a+x mongod

You can start mongodb by running the mongod script on your project root:

    ./mongod

To verify that mongod is installed, you can try  

    mongod --version. 

More details [here](https://community.c9.io/t/setting-up-mongodb/1717)