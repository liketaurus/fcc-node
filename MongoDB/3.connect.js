var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;

var url = 'mongodb://localhost:27017/learnyoumongo';
var mongo = require('mongodb');
var param = parseInt(process.argv[2]);

mongo.MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    }
    else {
        //console.log('Connection established to', url);
        db.collection('parrots').find({
            age: {
                $gt: param
            }
        }).toArray(function(err, docs) {
            if (err) throw err;
            console.log(docs);
            db.close();
        })
    }
});