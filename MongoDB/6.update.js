var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var database = process.argv[2];
var url = 'mongodb://localhost:27017/' + database;

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    }
    else {
        //console.log('Connection established to', url);
        db.collection('users').update({
            username: "tinatime"
        }, {
            $set: {
                age: 40
            }
        }, function(err, data) {
            if (err) throw err
            else {
                db.close();
            }
        })
    }
});