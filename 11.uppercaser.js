 var net = require('http');
 var map = require('through2-map');
 var port = parseInt(process.argv[2]);


 var server = net.createServer(function(request, response) {
     if (request.method != 'POST')
         return response.end('This server responds only to POST requests!');
     else
         request.pipe(map(function(data) {
             return data.toString().toUpperCase();
         })).pipe(response);
 });
 server.listen(port);