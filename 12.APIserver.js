 var net = require('http');
 var url = require('url');
 var port = parseInt(process.argv[2]);

 function unixtime(time) {
     return {
         "unixtime": time.getTime()
     };
 }

 function parsetime(time) {
     return {
         "hour": time.getHours(),
         "minute": time.getMinutes(),
         "second": time.getSeconds()
     };
 }

 var server = net.createServer(function(request, response) {
     var parsed = url.parse(request.url, true);
     var time = new Date(parsed.query.iso);
     var result;

     if (request.url.search('parsetime') != -1) {
         result = parsetime(time);
     }
     else if (request.url.search('unixtime') != -1) {
         result = unixtime(time);
     }

     if (result) {
         response.writeHead(200, {
             'Content-Type': 'application/json'
         });
         response.end(JSON.stringify(result));
     }
     else {
         response.writeHead(400);
         response.end();
     }
 });
 server.listen(port);