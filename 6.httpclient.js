var http = require("http");
var url = process.argv[2];

function callback(response) {
    response.setEncoding('utf8');
    response.on('data', function(data) {
        console.log(data);
    })
    response.on('error', function(data) {
        console.log('Error getting responce!');
    })
    response.on('end', function(data) {
        console.log('');
    })
}

http.get(url, callback);