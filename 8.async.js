var http = require("http");
var urls = process.argv.slice(2);
var results = [];
var num = 0;

urls.forEach(getContent);

function getContent(url, i) {
    http.get(url, function(response) {
        response.setEncoding('utf8');
        var s = '';
        response.on('data', function(data) {
            s = s + data;
        })
        response.on('error', function(data) {
            console.log('Error getting response!');
        })
        response.on('end', function(data) {
            results[i] = s;
            num++;
            if (num == 3) {
                results.forEach(function(text) {
                    console.log(text);
                })
            }
        })
    });
};