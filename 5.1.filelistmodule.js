

module.exports = function(dir, ext, callback) {
    var fs = require('fs');
    var path = require('path');
    var r = [];
    fs.readdir(dir, function(err, list) {
        if (!err) {
            var b = list;
            for (var i = 0; i < b.length; i++) {
                if (path.extname(b[i]) === '.' + ext)
                    r.push(b[i]);
            }
            return callback (null,r);
        }
        else
            return callback(err);
    });
    return r;
}